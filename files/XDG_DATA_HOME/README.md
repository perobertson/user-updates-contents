# XDG_DATA_HOME

- Where user-specific data files should be written (analogous to `/usr/share`).
- Should default to `$HOME/.local/share`.
