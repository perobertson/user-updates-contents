# XDG_CONFIG_HOME

- Where user-specific configurations should be written (analogous to `/etc`).
- Should default to `$HOME/.config`.
