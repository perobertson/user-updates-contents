# XDG_CACHE_HOME

- Where user-specific non-essential (cached) data should be written (analogous to `/var/cache`).
- Should default to `$HOME/.cache`.
