# XDG_RUNTIME_DIR

- Used for non-essential, user-specific data files such as sockets, named pipes, etc.
- Not required to have a default value; warnings should be issued if not set or equivalents provided.
- Must be owned by the user with an access mode of `0700`.
- Filesystem fully featured by standards of OS.
- Must be on the local filesystem.
- May be subject to periodic cleanup.
- Modified every 6 hours or set sticky bit if persistence is desired.
- Can only exist for the duration of the user's login.
- Should not store large files as it may be mounted as a tmpfs.
- pam_systemd sets this to `/run/user/$UID`.
